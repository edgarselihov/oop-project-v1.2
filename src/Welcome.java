import javax.swing.*;
import java.awt.*;
import java.io.IOException;

class Welcome {
    // welcome window
    private String inputVal;

    private Welcome() {

        JFrame f;
        JLabel description, descForInput;
        final JTextField tf;
        JButton b;
        JPanel panel, panel1, panel2;

        f = new JFrame("Welcome");//creating instance of JFrame
        description = new JLabel("Quiz: \"Who is real?\""); // label of game on welcomescreen
        descForInput = new JLabel("Enter username: ", JLabel.CENTER); // text above textbox
        tf = new JTextField(); // textbox
        b = new JButton("Submit");//creating instance of JButton

        //Title
        panel = new JPanel();
        panel.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        description.setFont(new Font("Arial", Font.BOLD, 30));
        panel.add(description);

        //Input
        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        panel1.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        panel1.add(descForInput, BorderLayout.CENTER);
        tf.setPreferredSize(new Dimension(150, 30));
        panel1.add(tf, BorderLayout.SOUTH);

        //Button
        panel2 = new JPanel();
        panel2.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        panel2.add(b);


        // action listener
        b.addActionListener(e -> {

            // if textbox is empty
            if (tf.getText().equals("") || tf.getText() == null) {

                JOptionPane.showMessageDialog(f,"Cannot insert empty username");

            }
            // if not
            else {
                // saving username
                inputVal = tf.getText();
                JOptionPane.showMessageDialog(f, "Welcome, " + inputVal);
                f.dispose();
                // create an object of username
                saveUsername user = new saveUsername(inputVal);
                // pass it further
                new menuWindow(user);

            }
        });
        f.setLayout(new BorderLayout());
        f.add(panel, BorderLayout.NORTH ); // add title
        f.add(panel1, BorderLayout.CENTER); // add text field
        f.add(panel2, BorderLayout.SOUTH); // add butto

        f.setSize(400, 500);//400 width and 500 height
        f.setVisible(true);//making the frame visible
        f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        f.setLocationRelativeTo(null);

    }

    public static void main(String[] args) {

        new Welcome();
        /*
        saveFile file = new saveFile("scores.txt");
        System.out.println("Prindin scores aknas ");
        try {
            file.returnTop();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

         */

    }
}