import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

class Scores {


    Scores(saveUsername player) throws IOException {

        saveFile file = new saveFile("scores.txt");
        Map<String, Integer> map = file.returnTop();

        ArrayList<String> names = new ArrayList<>();
        ArrayList<Integer> score = new ArrayList<>();
        JFrame scores;
        JLabel description;
        JTable table;
        JPanel panel, panel2, panel3;
        JButton button;
        scores = new JFrame("Top 5");
        //scores.setSize(600, 600);

        //Title
        description = new JLabel("Top 5 players", SwingConstants.CENTER);
        description.setFont(new Font("Arial", Font.PLAIN, 30));

        button = new JButton("Got it!");
        button.setPreferredSize(new Dimension(100, 40));

        //Create table with scores
        panel = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        panel2.add(button);
        panel2.setBorder((BorderFactory.createEmptyBorder(10, 10,30, 10 )));
        panel3.setBorder((BorderFactory.createEmptyBorder(30, 10,20, 10 )));
        //Sort hashmap in descending order and add to lists
        Map<String, Integer> sortedMap = map.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            names.add(entry.getKey());
            score.add(entry.getValue());
        }

        //Create table
        DefaultTableModel model = new DefaultTableModel();
        table = new JTable(model);

        // Create a couple of columns
        model.addColumn("Name");
        model.addColumn("Score");
        table.setRowHeight(40);
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setPreferredWidth(50);
        table.setFont(new Font("Arial", Font.PLAIN, 30));

        //Add table to scrollpane
        //JScrollPane scrollpane = new JScrollPane(table);
        //scrollpane.setPreferredSize(new Dimension(480, 300));
        //scrollpane.setViewportView(table);
        panel.add(BorderLayout.CENTER, table);
        panel2.add(button);
        panel3.add(description);


        // Append a rows
        if (names.size() < 5) {
            for(int i = 0; i< names.size(); i++){
                model.addRow(new Object[]{names.get(i), score.get(i)});
            }
        } else {
            for(int i = 0; i< 5; i++){
                model.addRow(new Object[]{names.get(i), score.get(i)});
            }
        }


        button.addActionListener(e -> {
            scores.dispose();
            new menuWindow(player);

        });
        scores.setLayout(new BorderLayout());
        scores.add(panel, BorderLayout.CENTER);
        scores.add(panel3, BorderLayout.NORTH);
        scores.add(panel2, BorderLayout.SOUTH);
        scores.pack();

        scores.setVisible(true);
        scores.setLocationRelativeTo(null);



        }
    }

