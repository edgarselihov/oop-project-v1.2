import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class saveFile {

    private saveUsername userName;
    private int userScore;
    private String fileName;


    public saveFile(String fileName) {
        this.fileName = fileName;
    }

    //Write to file username + score
    public void writeToFile(saveUsername userName, int score) throws IOException {
        File f = new File(fileName);
        try (Writer writer = new BufferedWriter(new FileWriter(f, true))) {
            writer.append(userName + ";" + score + System.lineSeparator());
        }
    }

    //read lines from file and return sorted in descending order
    public Map<String, Integer> returnTop() throws IOException {
        File f = new File(fileName);
        Map<String, Integer> map;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"))
        ) {
            map = new HashMap<>();
            String line = br.readLine();
                while (line != null) {
                    String[] lines = line.split(";");
                    map.put(lines[0], Integer.valueOf(lines[1]));
                    line = br.readLine();
                }
            }
        return map;
    }
}

