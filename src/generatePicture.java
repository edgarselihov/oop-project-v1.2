import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

public class generatePicture implements ActionListener {

    private int timeLeft, curScore, counter = 5;

    private JButton button1, button2, button3, button4;

    private JFrame frame;
    private JLabel score, countdown;
    private JPanel panel1, panel2, panel3, panel4, panel5, panel6;
    private JPanel[] listOfPanels;

    private ArrayList<BufferedImage> fakeImages, realImages;

    generatePicture (saveUsername player) {

        System.out.println(player.getUsername());
        //two separate directives for fake and real photos
        File dir_fake = new File("fake");
        File dir_real = new File("real");

        fakeImages = new ArrayList<>();
        realImages = new ArrayList<>();

        timeLeft = 11;
        curScore = 8;

        BufferedImage fakeImg;
        BufferedImage realImg;

        // for adding pictures of fake people
        for (final File f : Objects.requireNonNull(dir_fake.listFiles())) {

            try {

                fakeImg = ImageIO.read(f);
                fakeImages.add(fakeImg);

            } catch (final IOException e) {

                System.out.println("Pictures not found");

            }
        }
        // for adding pictures of real people
        for (final File f : Objects.requireNonNull(dir_real.listFiles())) {

            try {

                realImg = ImageIO.read(f);
                realImages.add(realImg);

            } catch (final IOException e) {

                System.out.println("Pictures not found");

            }
        }

        ArrayList<Integer> intElements;
        ArrayList<Integer> uniqueElements;

        intElements = returnListOfnumbers();
        System.out.println(intElements);

        // return arraylist with unique elements
        uniqueElements = returnUnique.returnUniqueList(intElements, fakeImages);
        System.out.println(uniqueElements);

        // apply the image to the button

        button1 = new JButton(new ImageIcon((fakeImages.get(uniqueElements.get(0))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button2 = new JButton(new ImageIcon((fakeImages.get(uniqueElements.get(1))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button3 = new JButton(new ImageIcon((fakeImages.get(uniqueElements.get(2))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button4 = new JButton(new ImageIcon((realImages.get(RandomNumber.returnRandomNumber(realImages.size() - 1))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        //teen frami ja paneeli
        frame = new JFrame("Quiz: \"Who is real?\"");
        countdown = new JLabel("Time: ");
        score = new JLabel("Score: " + curScore);

        //määran ära, mitu pilti korraga
        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel(new GridLayout(2, 2));
        panel4 = new JPanel();
        panel5 = new JPanel();
        panel6 = new JPanel();

        listOfPanels = new JPanel[4]; //massiv labelite jaoks
        // score alignment
        score.setBounds(20, 155, 100, 50);
        score.setFont(new Font("Arial", Font.PLAIN, 20));
        score.setPreferredSize(new Dimension(130, 60));
        // time alignment
        countdown.setBounds(20, 195, 100, 50);
        countdown.setFont(new Font("Arial", Font.PLAIN, 20));
        countdown.setPreferredSize(new Dimension(130, 60));

        // timer

        final Timer timer = new Timer(1000, evt -> {
            // set text with every "iteration"
            timeLeft--;
            countdown.setText("Time: " + timeLeft);

            System.out.println(timeLeft);
            // using the event object to get the source and as the result, stop timer if condition is met
            if (timeLeft <= 0) {

                ((Timer) evt.getSource()).stop();
                timeLeft = 0;

                    popupWindow(player);



            }
        });

        // action listener for clicking pictures (if true, then timer increases)

        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button4.addActionListener(this);

        timer.start();

        // adding pictures to JPanel

        button1.setAlignmentX(Component.CENTER_ALIGNMENT);
        button2.setAlignmentX(Component.CENTER_ALIGNMENT);
        button3.setAlignmentX(Component.CENTER_ALIGNMENT);
        button4.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel1.add(button1);
        panel5.add(button2);
        panel1.validate(); // validate picture for normal displaying on screen
        panel5.validate();

        panel2.add(button3);
        panel6.add(button4);
        panel2.validate();
        panel6.validate();

        panel3.setBorder(new EmptyBorder(50, 0, 50, 50));

        // create array

        listOfPanels[0] = panel1;
        listOfPanels[1] = panel5;
        listOfPanels[2] = panel2;
        listOfPanels[3] = panel6;

        randomizePosition(listOfPanels);

    }

    public void actionPerformed(ActionEvent e) {

        // create timers for blinking background
        //if correct, it blinks with green color
        Timer correct = new Timer(20, evt ->{

            counter--;
            panel3.setBackground(Color.green);
            panel4.setBackground(Color.green);

            if(counter <= 0) {

                ((Timer) evt.getSource()).stop(); // stop timer from inside
                panel3.setBackground(null);
                panel4.setBackground(null);

            }
        });
        //if wrong, it blinks with red color
        Timer wrong = new Timer(20, evt -> {

            counter--;
            panel3.setBackground(Color.red);
            panel4.setBackground(Color.red);

            if(counter <= 0) {

                ((Timer) evt.getSource()).stop();
                panel3.setBackground(null);
                panel4.setBackground(null);

            }
        });

        // method for updating pictures and updating time/score values
        if (e.getSource() == button4) {

            correct.start(); // start timer
            timeLeft += 3;
            curScore += 5;

        } else {

            wrong.start();
            timeLeft -= 3;
            curScore -= 5;

        }

        ArrayList<Integer> intElements;
        ArrayList<Integer> uniqueElements;

        intElements = returnListOfnumbers();
        System.out.println(intElements);

        // return arraylist with unique elements
        uniqueElements = returnUnique.returnUniqueList(intElements, fakeImages);
        System.out.println(uniqueElements);
        // call randomly new pictures
        button1.setIcon(new ImageIcon((fakeImages.get(uniqueElements.get(0))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button2.setIcon(new ImageIcon((fakeImages.get(uniqueElements.get(1))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button3.setIcon(new ImageIcon((fakeImages.get(uniqueElements.get(2))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        button4.setIcon(new ImageIcon((realImages.get(RandomNumber.returnRandomNumber(realImages.size() - 1))
                .getScaledInstance(250, 250, Image.SCALE_FAST))));

        score.setText("Score: " + curScore);
        counter = 5; // redefine counter again
        randomizePosition(listOfPanels);
    }

    private void randomizePosition(JPanel[] arrOfPanels) {
        // shuffles pictures and places them on main panel
        Collections.shuffle(Arrays.asList(arrOfPanels));

        for (JPanel arrOfPanel : arrOfPanels) {

            panel3.add(arrOfPanel);

        }

        panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));
        panel4.setLayout(new BoxLayout(panel4, BoxLayout.Y_AXIS));
        panel5.setLayout(new BoxLayout(panel5, BoxLayout.Y_AXIS));
        panel6.setLayout(new BoxLayout(panel6, BoxLayout.Y_AXIS));

        panel4.setBorder((BorderFactory.createEmptyBorder(100, 50, 150, 50)));
        panel4.add(Box.createRigidArea(new Dimension(30, 0)));

        panel4.add(score);
        panel4.add(countdown);

        frame.add(panel3, BorderLayout.CENTER);
        frame.add(panel4, BorderLayout.WEST);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void popupWindow(saveUsername player) {

        String[] buttons = {"Quit", "Play Again"};
        // popup window
        int popup = JOptionPane.showOptionDialog (

                frame,
                "GAME OVER\nYour Score:" + curScore +  "\nPlay again?",
                "GAME OVER",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                buttons,
                buttons[0]

        );
        // restarts game
        if (popup == 0) {

            System.out.println("quit");
            saveFile file = new saveFile("scores.txt");
            try {
                file.writeToFile(player, curScore);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // here will be passed user nickname and score to write into txt file
            frame.dispose();
            new menuWindow(player);

        }
        // opens main menu
        if (popup == 1) {

            frame.dispose(); // close main frame
            new generatePicture(player);

        }
    }

    private ArrayList<Integer> returnListOfnumbers() {

        ArrayList<Integer> intElements = new ArrayList<>();

        // adding random numbers to arraylist intElements
        for (int i = 0; i < 3; i++) {

            intElements.add(RandomNumber.returnRandomNumber(fakeImages.size() - 1));

        }

        return intElements;
    }

    // for testing purposes
    /*

    public static void main(String[] args) {

        saveUsername player = new saveUsername("ed");
        new generatePicture(player);

    }

     */
}
