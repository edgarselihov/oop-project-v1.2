import java.util.Random;

class RandomNumber {

    static int returnRandomNumber(int n) {
        // range from 1 to n
        Random rand = new Random();

        return rand.nextInt(n - 1) + 1;
    }
}
