import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

class menuWindow {

    menuWindow(saveUsername username) {

        JFrame menu;
        JLabel description, startGame, scores, exitGame, tutorial;
        JPanel panel1, panel2;
        Box box;

        menu = new JFrame("Main menu");
        menu.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        description = new JLabel("Quiz: \"Who is real?\""); // label of game on screen
        tutorial = new JLabel("Tutorial");
        startGame = new JLabel("Start Game");
        scores = new JLabel("Scores");
        exitGame = new JLabel("Exit");

        //Title
        panel1 = new JPanel();
        panel1.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        description.setFont(new Font("Arial", Font.BOLD, 30));
        panel1.add(description);


        //Choices
        panel2 = new JPanel();
        box = Box.createVerticalBox();
        panel2.setBorder((BorderFactory.createEmptyBorder(50, 50,50, 50 )));
        tutorial.setFont(new Font("Arial", Font.BOLD, 20));
        tutorial.setAlignmentX(Component.CENTER_ALIGNMENT);
        startGame.setFont(new Font("Arial", Font.BOLD, 20));
        startGame.setAlignmentX(Component.CENTER_ALIGNMENT);
        scores.setFont(new Font("Arial", Font.BOLD, 20));
        scores.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitGame.setFont(new Font("Arial", Font.BOLD, 20));
        exitGame.setAlignmentX(Component.CENTER_ALIGNMENT);

        box.add(tutorial);
        box.add(Box.createVerticalStrut(20));
        box.add(startGame);
        box.add(Box.createVerticalStrut(20));
        box.add(scores);
        box.add(Box.createVerticalStrut(20));
        box.add(exitGame);

        panel2.add(box);
/*
        tutorial.setBounds(150, 120, 150, 50);
        tutorial.setFont(new Font("Arial", Font.BOLD, 20));

        startGame.setBounds(150, 160, 150, 50);
        startGame.setFont(new Font("Arial", Font.BOLD, 20));

        scores.setBounds(150, 200, 150, 50);
        scores.setFont(new Font("Arial", Font.BOLD, 20));

        exitGame.setBounds(150, 240, 150, 50);
        exitGame.setFont(new Font("Arial", Font.BOLD, 20));

 */

        tutorial.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                menu.dispose();
                new Introduction(username);
                System.out.println("Opening tutorial...");

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // if start game is clicked
        startGame.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                menu.dispose();
                new generatePicture(username);
                System.out.println("Opening main game...");

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // if scores is clicked
        scores.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                menu.dispose();
                try {
                    new Scores(username);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        // IF exit is pressed
        exitGame.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // kill it for fuck sake
                System.out.println("exit");
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });


        menu.setLayout(new BorderLayout());
        menu.add(panel1, BorderLayout.NORTH);
        menu.add(panel2, BorderLayout.CENTER);
        menu.setSize(400, 500);
/*
        menu.add(tutorial);
        menu.add(description);
        menu.add(startGame);
        menu.add(scores);
        menu.add(exitGame);

 */

        //menu.setLayout(null);
        menu.setVisible(true);
        menu.setLocationRelativeTo(null);
        System.out.println(username.getUsername());

    }
}